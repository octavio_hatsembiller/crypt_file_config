#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

from class_file import FileConfig
command = FileConfig()

parser = argparse.ArgumentParser()
parser.add_argument("-sf", "--set_file_dat", help="create file", action="store_true")
parser.add_argument("-pi", "--print_info", help="Nombre de archivo a procesar", action="store_true")
parser.add_argument("-sif", "--set_info_file", help="Set info file", action="store_true")
parser.add_argument("-gkv", "--get_key_value", help="Get key value", action="store_true")
args = parser.parse_args()

# Aquí procesamos lo que se tiene que hacer con cada argumento
if args.set_file_dat:
    command.set_file_dat()

if args.print_info:
    command.print_info()

if args.get_key_value:
    print('Get key value ')
    info = input('Key: ')
    print(command.get_key_value(info))

if args.set_info_file:
    print('set key value ')
    key = input('Key: ')
    value = input('value: ')
    print(command.set_info_file(key, value))
